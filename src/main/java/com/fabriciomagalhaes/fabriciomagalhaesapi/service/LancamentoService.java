package com.fabriciomagalhaes.fabriciomagalhaesapi.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabriciomagalhaes.fabriciomagalhaesapi.model.Lancamento;
import com.fabriciomagalhaes.fabriciomagalhaesapi.model.Pessoa;
import com.fabriciomagalhaes.fabriciomagalhaesapi.repository.LancamentoRepository;
import com.fabriciomagalhaes.fabriciomagalhaesapi.repository.PessoaRepository;
import com.fabriciomagalhaes.fabriciomagalhaesapi.service.exception.PessoaInexistenteOuInativaException;

@Service
public class LancamentoService {
	
	@Autowired
	private PessoaRepository pessoaRepository;
	@Autowired
	private LancamentoRepository lancamentoRepository;

	public Lancamento salvar(Lancamento lancamento) {
		validarPessoa(lancamento);		
		return lancamentoRepository.save(lancamento);
	}
	
	public Lancamento atualizar(Long codigo, Lancamento lancamento) {
		Lancamento lancamentoSalvo = bucaLancamentoExixtente(codigo);
		if(!(lancamento.getPessoa()==lancamentoSalvo.getPessoa())) {
			validarPessoa(lancamento);
		}
		BeanUtils.copyProperties(lancamento, lancamentoSalvo);
		
		return lancamentoRepository.save(lancamentoSalvo);
	}

	private Lancamento bucaLancamentoExixtente(Long codigo) {
		Lancamento lancamentoSalvo = lancamentoRepository.getOne(codigo);
		if(lancamentoSalvo==null) {
			throw new IllegalArgumentException();
		}
		return lancamentoSalvo;
	}

	private void validarPessoa(Lancamento lancamento) {
		Pessoa pessoa = pessoaRepository.findOne(lancamento.getPessoa().getCodigo());
		if (pessoa == null || pessoa.isInativo()) {
			throw new PessoaInexistenteOuInativaException();
		}
	}

}
