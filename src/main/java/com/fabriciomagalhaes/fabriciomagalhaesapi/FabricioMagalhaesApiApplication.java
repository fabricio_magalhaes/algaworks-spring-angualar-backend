package com.fabriciomagalhaes.fabriciomagalhaesapi;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.fabriciomagalhaes.fabriciomagalhaesapi.config.property.FabricioMoneyApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(FabricioMoneyApiProperty.class)
public class FabricioMagalhaesApiApplication {
	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(FabricioMagalhaesApiApplication.class, args);
	}
}
