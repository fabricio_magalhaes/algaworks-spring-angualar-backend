package com.fabriciomagalhaes.fabriciomagalhaesapi.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.fabriciomagalhaes.fabriciomagalhaesapi.model.Lancamento;
import com.fabriciomagalhaes.fabriciomagalhaesapi.repository.filter.LancamentoFilter;
import com.fabriciomagalhaes.fabriciomagalhaesapi.repository.projection.ResumoLancamento;

public interface LancamentoRepositoryQuery {
	
	public Page<Lancamento> filtrar(LancamentoFilter lancamentoFilter, Pageable pageable);
	public Page<ResumoLancamento> resumir(LancamentoFilter lancamentoFilter, Pageable pageable);

}
